#include <iostream>
#include <cmath>
#include <functional>
#include <thread>
#include <mutex>
#include <ros/ros.h>
#include <gazebo/gazebo.hh>
#include <ignition/math.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <std_msgs/Float32.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/PoseStamped.h>
#include <tf/transform_broadcaster.h>


class ShipStatePlugin : public gazebo::ModelPlugin {
public:
  ShipStatePlugin() : gazebo::ModelPlugin() {
    std::cout << "Starting ShipStatePlugin" << std::endl;
  }
  
  virtual ~ShipStatePlugin() {
    std::cout << "Closing ShipStatePlugin" << std::endl;
    delete _nh;
  }

  void Load(gazebo::physics::ModelPtr parent, sdf::ElementPtr sdf) {
    _model = parent;
    
    if (sdf->HasElement("rate")) {
      _rate = sdf->Get<double>("rate");
    } else {
      _rate = 30.0;
    }
    
    if (sdf->HasElement("base_link")) {
      _base_frame = sdf->Get<std::string>("base_link");
      _base_link = _model->GetLink(_base_frame);
    }
    
    if (sdf->HasElement("footprint_frame")) {
      _footprint_frame = sdf->Get<std::string>("footprint_frame");
    }
    
    if (!ros::isInitialized()) {
      int argc = 0;
      char** argv = NULL;
      ros::init(argc, argv, "ship_state_plugin", ros::init_options::NoSigintHandler);
    }
    
    _joints = _model->GetJoints();
    
    _nh = new ros::NodeHandle("");
    _state_pub = _nh->advertise<sensor_msgs::JointState>("joint_states", 100);
    _pose_pub = _nh->advertise<geometry_msgs::PoseStamped>("boat_pose", 100);
    _angle_pub = _nh->advertise<std_msgs::Float32>("boat_angle", 100);
    _ros_thread = std::thread(std::bind(&ShipStatePlugin::rosThread, this));
    _updateConnection = gazebo::event::Events::ConnectWorldUpdateBegin(std::bind(&ShipStatePlugin::onUpdate, this));
  }
  
  void onUpdate() {
  }
  
  void rosThread() {
    ros::Rate rate(_rate);
    while (ros::ok()) {
      ros::spinOnce();
      
      /* publish joint states */
      sensor_msgs::JointState msg;
      msg.header.stamp = ros::Time::now();
      
      for (const auto& j : _joints) {
        msg.name.push_back(j->GetName());
        msg.position.push_back(j->Position(0));
      }
      
      _state_pub.publish(msg);
      
      /* broadcast ship pose */
      ignition::math::Pose3d world_pose = _base_link->WorldPose();
      ignition::math::Quaterniond world_rot = world_pose.Rot();
      ignition::math::Vector3d world_pos = world_pose.Pos();
      ignition::math::Vector3d world_euler = world_rot.Euler();
      
      tf::Transform T_footprint;
      T_footprint.setOrigin( tf::Vector3(world_pos.X(), world_pos.Y(), 0.0) );
      tf::Quaternion q;
      q.setRPY(0, 0, world_euler.Z());
      T_footprint.setRotation(q);
      _br.sendTransform(tf::StampedTransform(T_footprint, ros::Time::now(), "world", _footprint_frame));
      
      tf::Transform T_base;
      T_base.setOrigin( tf::Vector3(0, 0, 0) );
      tf::Quaternion q1;
      q1.setRPY(world_euler.X(), world_euler.Y(), 0);
      T_base.setRotation(q1);
      _br.sendTransform(tf::StampedTransform(T_base, ros::Time::now(), _footprint_frame, _base_frame));
      
      /* publish ship pose */
      geometry_msgs::PoseStamped pose_msg;
      pose_msg.header.stamp = ros::Time::now();
      
      pose_msg.pose.position.x = world_pos.X();
      pose_msg.pose.position.y = world_pos.Y();
      pose_msg.pose.position.z = world_pos.Z();
      pose_msg.pose.orientation.x = world_rot.X();
      pose_msg.pose.orientation.y = world_rot.Y();
      pose_msg.pose.orientation.z = world_rot.Z();
      pose_msg.pose.orientation.w = world_rot.W();
      
      _pose_pub.publish(pose_msg);
      
      /* publish ship angle */
      std_msgs::Float32 angle_msg;
      angle_msg.data = (world_euler.Z());
      _angle_pub.publish(angle_msg);
      
      rate.sleep();
    }
  }

private:
  ros::NodeHandle* _nh;
  std::thread _ros_thread;
  double _rate;
  ros::Publisher _state_pub;
  ros::Publisher _pose_pub;
  ros::Publisher _angle_pub;
  gazebo::physics::ModelPtr _model;
  gazebo::event::ConnectionPtr _updateConnection;
  gazebo::physics::Joint_V _joints;
  std::string _base_frame;
  gazebo::physics::LinkPtr _base_link;
  std::string _footprint_frame;
  tf::TransformBroadcaster _br;
};
  
GZ_REGISTER_MODEL_PLUGIN(ShipStatePlugin)
