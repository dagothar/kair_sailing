#include <iostream>
#include <cmath>
#include <functional>
#include <thread>
#include <mutex>
#include <ros/ros.h>
#include <gazebo/gazebo.hh>
#include <ignition/math.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <geometry_msgs/Vector3.h>
#include <cmath>


namespace {
  double angle(const ignition::math::Vector3d& a, const ignition::math::Vector3d& b) {
    return acos(a.Dot(b) / (a.Length() * b.Length()));
  }
  
  double CD(double alpha, double cd_0, double cd_coeff) {
    double a = fabs(alpha);
    if (a > M_PI/2) {
      a = M_PI - a;
    }
    
    return cd_0 + cd_coeff * a*a;
  }
  
  double CL(double alpha, double a0, double ap, double cl_max) {
    double a = fabs(alpha);
    double s = 1.0;
    if (a > M_PI/2) {
      a = M_PI - a;
      s = -1.0;
    }
    
    double cl = 0.0;
    
    if (a > a0 && a < ap) {
      cl = (a-a0) * cl_max / (ap - a0);
    } else if (a > ap && a < M_PI/2) {
      cl = cl_max - (a-ap) * cl_max / (M_PI/2 - ap);
    } else {
      cl = 0.0;
    }
    
    return cl * s;
  }
};


class FoilPlugin : public gazebo::ModelPlugin {
public:
  FoilPlugin() : gazebo::ModelPlugin() {
    gzmsg << "Starting FoilPlugin" << std::endl;
  }
  
  virtual ~FoilPlugin() {
    gzmsg << "Closing FoilPlugin" << std::endl;
    delete _nh;
  }

  void Load(gazebo::physics::ModelPtr parent, sdf::ElementPtr sdf) {
    _model = parent;
    
    if (sdf->HasElement("foil")) {
      std::string foil_name = sdf->Get<std::string>("foil");
      _foil = _model->GetLink(foil_name);
      if (!_foil) {
        gzerr << "Foil link [" << foil_name << "] not found!" << std::endl;
      }
    }
    
    std::string wind_topic;
    if (sdf->HasElement("wind_topic")) {
      wind_topic = sdf->Get<std::string>("wind_topic");
    }
    
    if (sdf->HasElement("area")) {
      _area = sdf->Get<double>("area");
    }
    
    if (sdf->HasElement("center")) {
      _center = sdf->Get<ignition::math::Vector3d>("center");
      gzdbg << "Pressure center: " << _center << std::endl;
    }
    
    if (sdf->HasElement("density")) {
      _density = sdf->Get<double>("density");
    }
    
    if (sdf->HasElement("alpha_0")) {
      _alpha_0 = sdf->Get<double>("alpha_0");
    }
    
    if (sdf->HasElement("alpha_peak")) {
      _alpha_peak = sdf->Get<double>("alpha_peak");
    }
    
    if (sdf->HasElement("cl_max")) {
      _cl_max = sdf->Get<double>("cl_max");
    }
    
    if (sdf->HasElement("cd_0")) {
      _cd_0 = sdf->Get<double>("cd_0");
    }
    
    if (sdf->HasElement("cd_coeff")) {
      _cd_coeff = sdf->Get<double>("cd_coeff");
    }
        
    if (!ros::isInitialized()) {
      int argc = 0;
      char** argv = NULL;
      ros::init(argc, argv, "foil_plugin", ros::init_options::NoSigintHandler | ros::init_options::AnonymousName);
    }
    
    _nh = new ros::NodeHandle("");
    _ros_thread = std::thread(std::bind(&FoilPlugin::rosThread, this));
    _wind_sub = _nh->subscribe(wind_topic, 100, &FoilPlugin::onWindCallback, this);
    _updateConnection = gazebo::event::Events::ConnectWorldUpdateBegin(std::bind(&FoilPlugin::onUpdate, this));
  }
  
  void onUpdate() {
    _wind_mtx.lock();
    ignition::math::Vector3d v_wind_world = _wind;
    _wind_mtx.unlock();
        
    auto wTfoil = _foil->WorldPose();
    auto wRfoil = wTfoil.Rot();
    auto foilRw = wRfoil.Inverse();
    auto v_foil_world = _model->WorldLinearVel();
        
    auto v_apparent_world = -(v_wind_world - v_foil_world);
    auto v_apparent_foil = foilRw.RotateVector(v_apparent_world);
    v_apparent_foil.Z() = 0;
    double wind_velocity = v_apparent_foil.Length();
            
    double alpha = atan2(v_apparent_foil.Y(), v_apparent_foil.X());
        
    double cd = CD(alpha, _cd_0, _cd_coeff);
    double drag_magnitude = 0.5 * cd * _density * wind_velocity * wind_velocity * _area;
    auto drag_direction = -v_apparent_foil.Normalize();
    auto drag_force = drag_magnitude * drag_direction;
    
    if (!isnan(drag_magnitude)) {
      _foil->AddLinkForce(drag_force, _center);
    } else {
      gzwarn << "Drag is nan!" << std::endl;
    }
        
    double cl = CL(alpha, _alpha_0, _alpha_peak, _cl_max);
    double lift_magnitude = 0.5 * cl * _density * wind_velocity * wind_velocity * _area;
    auto lift_direction = (alpha < 0.0 ? 1.0 : -1.0) * drag_direction.Cross(ignition::math::Vector3d::UnitZ);
    auto lift_force = lift_magnitude * lift_direction;
    
    if (!isnan(lift_magnitude)) {
      _foil->AddLinkForce(lift_force, _center);
    } else {
      gzwarn << "Lift is nan!" << std::endl;
    }
    
    //gzdbg << "a=" << alpha << " cl=" << cl << " cd=" << cd << " drag=" << drag_force_world << " lift=" << lift_force_world << std::endl;
    //gzdbg << "a=" << alpha << " cd=" << cd << " drag=" << drag_force_world << std::endl;
    //gzdbg << "lift=" << lift_force << std::endl;
    //gzdbg << "D=" << drag_force << std::endl;
    //gzdbg << "L=" << lift_force << std::endl;
  }
  
  void rosThread() {
    ros::MultiThreadedSpinner spinner(1);
    spinner.spin();
  }
  
  void onWindCallback(const geometry_msgs::Vector3::ConstPtr& msg) {
    _wind_mtx.lock();
    _wind.X() = msg->x;
    _wind.Y() = msg->y;
    _wind.Z() = msg->z;
    _wind_mtx.unlock();
  }

private:
  ros::NodeHandle* _nh;
  std::thread _ros_thread;
  std::mutex _wind_mtx;
  double _rate;
  ros::Subscriber _wind_sub;
  ignition::math::Vector3d _wind;
  ignition::math::Vector3d _center;
  gazebo::physics::ModelPtr _model;
  gazebo::physics::LinkPtr _foil;
  gazebo::event::ConnectionPtr _updateConnection;
  
  double _area;
  double _density;
  double _alpha_0;
  double _alpha_peak;
  double _cl_max;
  double _cd_0;
  double _cd_coeff;
};
  
GZ_REGISTER_MODEL_PLUGIN(FoilPlugin)
