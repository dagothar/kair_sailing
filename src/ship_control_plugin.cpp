#include <iostream>
#include <cmath>
#include <functional>
#include <thread>
#include <mutex>
#include <ros/ros.h>
#include <gazebo/gazebo.hh>
#include <ignition/math.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <std_msgs/Float32.h>


class ShipControlPlugin : public gazebo::ModelPlugin {
public:
  ShipControlPlugin() : gazebo::ModelPlugin(), _rudder_command(0.0), _sheet_command(0.0), _motors_command(0.0) {
    std::cout << "Starting ShipControlPlugin" << std::endl;
  }
  
  virtual ~ShipControlPlugin() {
    std::cout << "Closing ShipControlPlugin" << std::endl;
    delete _nh;
  }

  void Load(gazebo::physics::ModelPtr parent, sdf::ElementPtr sdf) {
    _model = parent;
    
    if (sdf->HasElement("rate")) {
      _rate = sdf->Get<double>("rate");
    } else {
      _rate = 30.0;
    }
    if (sdf->HasElement("motor_link")) {
      std::string hull_name = sdf->Get<std::string>("motor_link");
      _hull_link = _model->GetLink(hull_name);
    }
    if (sdf->HasElement("sail_joint")) {
      std::string sail_joint_name = sdf->Get<std::string>("sail_joint");
      _sail_joint = _model->GetJoint(sail_joint_name);
    }
    if (sdf->HasElement("rudder_joint")) {
      std::string rudder_joint_name = sdf->Get<std::string>("rudder_joint");
      _rudder_joint = _model->GetJoint(rudder_joint_name);
    }
    
    if (!ros::isInitialized()) {
      int argc = 0;
      char** argv = NULL;
      ros::init(argc, argv, "ship_control_plugin", ros::init_options::NoSigintHandler);
    }
    
    _nh = new ros::NodeHandle("");
    _ros_thread = std::thread(std::bind(&ShipControlPlugin::rosThread, this));
    _rudder_sub = _nh->subscribe("rudder", 100, &ShipControlPlugin::onRudderCommand, this);
    _sheets_sub = _nh->subscribe("sheets", 100, &ShipControlPlugin::onSheetCommand, this);
    _motors_sub = _nh->subscribe("motors", 100, &ShipControlPlugin::onMotorsCommand, this);
    _updateConnection = gazebo::event::Events::ConnectWorldUpdateBegin(std::bind(&ShipControlPlugin::onUpdate, this));
  }
  
  void onUpdate() {
    _rudder_mtx.lock();
    float rudder = _rudder_command;
    _rudder_mtx.unlock();
    
    _sheet_mtx.lock();
    float sheet = _sheet_command;
    _sheet_mtx.unlock();
    
    _rudder_joint->SetLowerLimit(0, rudder);
    _rudder_joint->SetUpperLimit(0, rudder);
    
    //_sail_joint->SetLowerLimit(0, -sheet);
    //_sail_joint->SetUpperLimit(0, +sheet);
    _sail_joint->SetLowerLimit(0, -sheet);
    _sail_joint->SetUpperLimit(0, +sheet);
    
    _hull_link->AddLinkForce(ignition::math::Vector3d(_motors_command, 0, 0));
  }
  
  void rosThread() {
    ros::Rate rate(_rate);
    while (ros::ok()) {
      ros::spinOnce();
      //publishDronePose();
      rate.sleep();
    }
  }
  
  void onRudderCommand(const std_msgs::Float32::ConstPtr& msg) {
    _rudder_mtx.lock();
    _rudder_command = msg->data;
    _rudder_mtx.unlock();
  }
  
  void onSheetCommand(const std_msgs::Float32::ConstPtr& msg) {
    _sheet_mtx.lock();
    _sheet_command = msg->data;
    _sheet_mtx.unlock();
  }
  
  void onMotorsCommand(const std_msgs::Float32::ConstPtr& msg) {
    _motors_mtx.lock();
    _motors_command = msg->data;
    _motors_mtx.unlock();
  }

private:
  ros::NodeHandle* _nh;
  std::thread _ros_thread;
  double _rate;
  ros::Subscriber _rudder_sub;
  ros::Subscriber _sheets_sub;
  ros::Subscriber _motors_sub;
  std::mutex _rudder_mtx;
  std::mutex _sheet_mtx;
  std::mutex _motors_mtx;
  float _rudder_command;
  float _sheet_command;
  float _motors_command;
  gazebo::physics::ModelPtr _model;
  gazebo::event::ConnectionPtr _updateConnection;
  gazebo::physics::JointPtr _sail_joint;
  gazebo::physics::JointPtr _rudder_joint;
  gazebo::physics::LinkPtr _hull_link;
};
  
GZ_REGISTER_MODEL_PLUGIN(ShipControlPlugin)
