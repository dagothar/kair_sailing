#!/usr/bin/python3

import rospy
from geometry_msgs.msg import Vector3
from dynamic_reconfigure.server import Server
from kair_sailing.cfg import WindConfig
import math


wind_direction = 0.0
wind_magnitude = 0.0


def wind_parameters_cb(config, level):
  global wind_direction, wind_magnitude
  wind_direction = config["wind_direction"]
  wind_magnitude = config["wind_magnitude"]
  return config


rospy.init_node("wind_manual")
rate_hz = rospy.get_param("~rate", 30.0)
srv = Server(WindConfig, wind_parameters_cb)
wind_pub = rospy.Publisher("wind", Vector3, queue_size=100)

rate = rospy.Rate(rate_hz)
while not rospy.is_shutdown():
  msg = Vector3()
  msg.x = wind_magnitude * math.cos(wind_direction)
  msg.y = wind_magnitude * math.sin(wind_direction)
  msg.z = 0.0
  wind_pub.publish(msg)
  rate.sleep()

