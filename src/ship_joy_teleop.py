#!/usr/bin/python3

import rospy
from sensor_msgs.msg import Joy
from std_msgs.msg import Float32

cmd_rudder = 0.0
cmd_sheets = 0.0
cmd_motors = 0.0
d_cmd_sheets = 0.0


rospy.init_node("ship_joy_teleop")

rudder_axis_index = rospy.get_param("~rudder_axis_index", 2)
sheets_axis_index = rospy.get_param("~sheets_axis_index", 1)
rudder_axis_multiplier = rospy.get_param("~rudder_axis_multiplier", 0.75)
sheets_axis_multiplier = rospy.get_param("~sheets_axis_multiplier", 0.75)
motors_axis_index = rospy.get_param("~motors_axis_index", 1)
motors_axis_multiplier = rospy.get_param("~motors_axis_multiplier", 10.0)
sheets_limit = rospy.get_param("~sheets_limit", 1.57)
rate_hz = rospy.get_param("~rate", 30.0)


rudder_pub = rospy.Publisher("rudder", Float32, queue_size=100)
sheets_pub = rospy.Publisher("sheets", Float32, queue_size=100)
motors_pub = rospy.Publisher("motors", Float32, queue_size=100)

def joy_cb(msg):
  global cmd_rudder, d_cmd_sheets, cmd_motors
  cmd_rudder = msg.axes[rudder_axis_index] * rudder_axis_multiplier
  d_cmd_sheets = msg.axes[sheets_axis_index] * sheets_axis_multiplier
  cmd_motors = msg.axes[motors_axis_index] * motors_axis_multiplier

joy_sub = rospy.Subscriber("joy", Joy, joy_cb)

rate = rospy.Rate(rate_hz)
while not rospy.is_shutdown():
  cmd_sheets += d_cmd_sheets / rate_hz
  if cmd_sheets > sheets_limit:
    cmd_sheets = sheets_limit
  if cmd_sheets < 0:
    cmd_sheets = 0
  
  rudder_pub.publish(cmd_rudder)
  sheets_pub.publish(cmd_sheets)
  motors_pub.publish(cmd_motors)
  
  try:
    rate.sleep()
  except:
    pass

