union() {
  
  for(x = [-10:10]) {
    translate([x, 0, 0]) rotate([90, 0, 0])
      cylinder(r=0.005, h=20, center=true, $fn=20);
  }
  
  for(y = [-10:10]) {
    translate([0, y, 0]) rotate([0, 90, 0])
      cylinder(r=0.01, h=20, center=true, $fn=20);
  }
}